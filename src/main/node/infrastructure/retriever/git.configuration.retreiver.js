//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.


/* global module */
/* global require */

'use strict';

const Git = require('nodegit');

const AbstractConfigurationRetriever = require('../abstract.configuration.retriever');

class GitConfigurationRetriever extends AbstractConfigurationRetriever {

  constructor({ applicationContext, environment }) {
    super(environment);
    this.applicationContext = applicationContext;
    this.repository = undefined;
    this.timer = undefined;
  }

  postConstruct() {
    this._setCredentialIfRequired();
    this.timer = setInterval(this.retrieve.bind(this), 300000);
  }

  preDestroy() {
    clearInterval(this.timer);
  }

  /**
   * Retrieve the configuration for the service from a local file
   */
  async retrieve() {
    await this._doPull();

    const commit = await this.repository.getHeadCommit();
    const entry = await commit.getEntry(`${this.environment.get('configuration:service_name')}-config.json`);
    const conf = await entry.getBlob();
    return this.environment.overrides(JSON.parse(conf.toString()));
  }

  _setCredentialIfRequired() {
    const credential = this.environment.get('configuration:credential');
    if(credential) {
      Git.Cred.userpassPlaintextNew(credential.username, credential.password);
    }
  }

  async _doPull() {
    if(!this.repository) {
      await this.clone();
    }
    await this.repository.fetchAll();
    await this.repository.mergeBranches("master", "origin/master");
  }

  async clone() {
    this.repository = await Git.Clone(this.environment.get('configuration:url'), `/tmp/dino-git-repo-${Date.now()}`, {
      fetchOpts: {
        callbacks: {
          certificateCheck: function () {
            // github will fail cert check on some OSX machines
            // this overrides that check
            return 0;
          }
        }
      }
    });
  }

}

module.exports = GitConfigurationRetriever;