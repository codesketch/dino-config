//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.


/* global module */
/* global require */
/* global process */

'use strict';

const AbstractConfigurationRetriever = require('../abstract.configuration.retriever');

class LocalConfigurationRetriever extends AbstractConfigurationRetriever {

  constructor({applicationContext, environment}) {
    super(environment);
    this.applicationContext = applicationContext;
  }

  /**
   * Retrieve the configuration for the service from a local file
   */
  retrieve() {
    return new Promise((resolve, reject) => {
      const path = `${process.cwd()}/${this.environment.get('configuration:path')}${this.environment.get('configuration:service_name')}-config`;
      try {
        const conf =  require(path);
        this.environment.overrides(conf);
        resolve(this.environment);
      } catch (error) {
        reject(error);
      }
    });
  }

}

module.exports = LocalConfigurationRetriever;