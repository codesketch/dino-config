//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const assert = require('assert');

const TestHelper = require('./TestHelper');

const ConfigurationRetreiverFactory = require('../../../main/node/infrastructure/configuration.retriever.factory');
const LocalConfifurationRetreiver = require('../../../main/node/infrastructure/retriever/local.configuration.retriever');

describe('ConfigurationRetreiverFactory - Integration Test', () => {

  const applicationContext = TestHelper.getApplicationContext();
  const testObj = new ConfigurationRetreiverFactory({ applicationContext});

  it('loads a valid retriever', () => {
    // act
    const localConfigurationRetriever = testObj.get('local');
    // assert
    assert.ok(localConfigurationRetriever instanceof LocalConfifurationRetreiver);
  });
});
