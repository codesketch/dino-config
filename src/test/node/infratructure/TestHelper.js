/* global require */
/* global module */
/* global process */

const Dino = require('dino-core');

process.env.DINO_CONTEXT_ROOT = 'src/main/node';
process.env.DINO_CONFIG_PATH = 'src/test/resources/test.config.json';

const applicationContext = Dino.run();

class TestHelper {

  static getApplicationContext() {
    return applicationContext;
  }
}

module.exports = TestHelper;