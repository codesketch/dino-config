DinO Express welcomes contributors to help grow functionalities for the community, 
not all functionalities are delivered by code and there is a work to do verifying 
issues, writing documentation, validating pull requests and so on.

Everyone is welcome to contribute to DinO Express!

## How to contribute

### Fix Bugs or provide new features
If you like to fix a bug or implement new functionalities please go to the [issues](https://gitlab.com/codesketch/dino-express/issues)
area and choose the issue you like to work on, the process to propose new code is as following:
 1) Create a new branch from master
 2) Implement your fix or your new feature on the branch you created, please provide unit test for your work, this will help reviewer and users DinO Express.
 3) Create a merge request for your branch, the implementatin will now be reviewed in a constructive environment
 4) Once your contribution is accepted the branch will be merged on master to be 

### Create or update documenation
If you like to create to create or update documentation please do so on the following areas:
 1) [README.md](https://gitlab.com/codesketch/dino-express/blob/master/README.md) file, information provided here should be quick and aimed to provide an effective while high level view of the features to the reader
 2) [Wiki](https://gitlab.com/codesketch/dino-express/wikis/home)

The process will be as following:
 1) Create your documentation 
 2) Ask for review creating a new issue
 3) Once the review is done and the contribution accepted it will be made public

### Validate merge requests
I you would like to validate merge requests please head to the [merge requests](https://gitlab.com/codesketch/dino-express/merge_requests) section and pick the merge request you would like to review.

For any other way you like to contribute please get in touch creating a new [issue](https://gitlab.com/codesketch/dino-express/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) of type request.
